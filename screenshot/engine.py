import logging
import multiprocessing
import os
import sys
import time
import urllib

from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.chrome.options import Options

import uuid


# logging.basicConfig(level=logging.DEBUG)


class Screenshot:
    def __init__(self, url_file, width=800, height=600, output_directory=None):
        self.service = Service('/usr/bin/chromedriver')
        self.service.start()

        if url_file == '-':
            self.urls = self.validate(sys.stdin.readlines())
        else:
            with open(url_file) as f:
                self.urls = self.validate(f.read().splitlines())

        self.width = width
        self.height = height
        self.output_directory = output_directory

        # Clean up this mess
        self.logger = logging.getLogger(__name__)
        if not self.logger.handlers:
            self.logger.setLevel(logging.DEBUG)
            ch = logging.StreamHandler()
            ch.setLevel(logging.WARN)
            self.logger.addHandler(ch)
            fh = logging.FileHandler('screenshot.log')
            fh.setLevel(logging.DEBUG)
            fh.setFormatter(
                logging.Formatter('%(asctime)s - %(levelname)s - %(process)d - %(filename)s:%(funcName)s - %(message)s',
                                  datefmt='%Y-%m-%d %H:%M:%S'))
            self.logger.addHandler(fh)

    def validate(self, urls):
        validated = []
        for url in urls:
            url = url.strip()

            if url.startswith('http'):
                validated.append(url)
            else:
                validated.append('https://' + url)

        return validated

    def source_exists(self, source, state):
        for s in state:
            if source == s:
                self.logger.debug('Source already exists in state')
                return True

    def add_source(self, source, state):
        state.append(source)

    def worker(self, queue, state):
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument("--window-size={},{}".format(self.width, self.height))
        driver = webdriver.Remote(self.service.service_url, options=chrome_options)

        while True:
            url = queue.get()

            # Look for the poison pill
            if url is None:
                queue.task_done()
                driver.quit()
                return

            driver.get(url)

            # TODO: Right now we sleep a bit so the site can render, could be improved
            time.sleep(2.5)

            source = driver.page_source

            if self.source_exists(source, state):
                # Deduplication
                queue.task_done()
                continue

            self.add_source(driver.page_source, state)

            if self.output_directory:
                if not os.path.exists(self.output_directory):
                    os.makedirs(self.output_directory)

            path = self.generate_path(url)

            driver.get_screenshot_as_file(path)

            self.logger.info('Took screenshot of {}'.format(url))

            queue.task_done()

    def generate_path(self, url):
        filename = self.url_to_filename(url)

        if self.output_directory:
            path = '{}/{}'.format(self.output_directory, filename)
        else:
            path = filename

        return path

    def run(self, num_drivers):
        q = multiprocessing.JoinableQueue()
        state = multiprocessing.Manager().list()

        for url in self.urls:
            q.put(url)

        logging.info('Queued {} requests'.format(len(self.urls)))

        for i in range(num_drivers):
            logging.info('Starting worker {}'.format(i))
            p = multiprocessing.Process(target=self.worker, args=(q, state))
            p.start()

        # Poison pill
        for i in range(num_drivers):
            q.put(None)

        q.join()

    def url_to_filename(self, url):
        url = urllib.parse.urlparse(url)
        return '{}.png'.format(url.netloc)
