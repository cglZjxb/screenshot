#!/usr/bin/env python

from distutils.core import setup

setup(name='screenshot',
      version='1.0',
      description='take a bunch of screenshots',
      author='William Söderberg',
      author_email='william.soderberg@gmail.com',
      url='',
      packages=['screenshot'],
      scripts=['scripts/screenshot'], requires=['selenium']
      )