## screenshot

Takes a list of URLs as input and produces screenshots. 

### Installation

Install with  `pip install .`

Chromedriver is a dependency, put `chromedriver` in `/usr/bin`.

### Example usage

The below command reads a masscan result, converts the result to URLs and pipes it into screenshot: 
```
masscan --readscan test.scan |  massconvert --input-t masscan --input-f - | screenshot --url-f - --output-dir screenshots
```

If you simply have a file with URLs, you can read it directly as well. The below command reads URLs from the file 
`urls.lst`, uses `5` processes for screenshots and writes the results into the output directory `screenshots`:  
```
screenshot --url-f urls.lst -n 5 --output-directory screenshots
``` 

### Known issues

* URLs which are not reachable, times out etc, results in white screenshots.
* If multiple URLs exist for the same URL network location, the previous screenshot file will overwritten
 